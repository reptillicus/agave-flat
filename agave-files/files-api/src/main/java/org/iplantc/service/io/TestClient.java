package org.iplantc.service.io;

import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.ContainerNotFoundException;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.blobstore.domain.BlobMetadata;
import org.jclouds.blobstore.domain.ContainerAccess;
import org.jclouds.blobstore.domain.PageSet;
import org.jclouds.blobstore.domain.StorageMetadata;
import org.jclouds.blobstore.domain.StorageType;
import org.jclouds.blobstore.options.CopyOptions;
import org.jclouds.blobstore.options.ListContainerOptions;
import org.jclouds.http.HttpException;
import org.jclouds.http.options.GetOptions;
import org.jclouds.blobstore.BlobStore;
import org.apache.log4j.Logger;

import java.util.List;

public class TestClient {
    public static final Logger log = Logger.getLogger(TestClient.class);

    public static void main(String[] args) throws Exception
    {
        long startTime = System.currentTimeMillis();
        BlobStoreContext context = ContextBuilder.newBuilder("s3")
                .endpoint("http://localhost:9000")
                .credentials("admin", "password")
                .buildView(BlobStoreContext.class);

        BlobStore blobStore = context.getBlobStore();
        blobStore.clearContainer("test");
        blobStore.createDirectory("test", "transfer/test_upload.bin");

        ListContainerOptions opts = new ListContainerOptions();
        opts.recursive();
        opts.inDirectory("transfer/test_upload.bin");
//        blobStore.deleteDirectory("test", "transfer/test_upload.bin")
        blobStore.clearContainer("test", opts);

        blobStore.deleteDirectory("test", "transfer/test_upload.bin");

        blobStore.createDirectory("test", "transfer");
    }
}
