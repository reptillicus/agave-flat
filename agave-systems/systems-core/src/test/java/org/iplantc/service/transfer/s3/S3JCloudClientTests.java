/**
 * 
 */
package org.iplantc.service.transfer.s3;

import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.model.StorageSystem;
import org.iplantc.service.transfer.exceptions.RemoteDataException;
import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.iplantc.service.transfer.s3.S3RemoteBaseTest;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author meiring
 *
 */
@Test(singleThreaded=true, groups= {"s3.filesystem","unit"})
public class S3JCloudClientTests
{
	private S3Jcloud client;

	@BeforeClass
	public void beforeClass() throws Exception {
		client  = new S3Jcloud(
				"username",
				"password",
				"/test",
				"/cat/dog",
				"test",
				"http://localhost",
				1234);
	}


	@Test(singleThreaded=true, groups= {"integration"})
	protected void testDoResolvedPath() {
		try {
			String res = client.resolvePath("/a/b");
			Assert.assertEquals(res, "test/cat/dog/a/b");
		} catch (Exception e) {
			Assert.fail("Did not resolve path properly");
		}
	}

}
