/**
 * 
 */
package org.iplantc.service.transfer.s3;

import org.iplantc.service.systems.dao.SystemDao;
import org.iplantc.service.systems.exceptions.RemoteCredentialException;
import org.iplantc.service.systems.model.StorageSystem;
import org.iplantc.service.transfer.AbstractRemoteDataClientTest;
import org.iplantc.service.transfer.RemoteDataClient;
import org.iplantc.service.transfer.exceptions.RemoteDataException;
import org.jclouds.blobstore.options.ListContainerOptions;
import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author meiring
 *
 */
public class S3RemoteBaseTest extends AbstractRemoteDataClientTest
{
	protected String containerName;


	@Override
	@BeforeMethod(alwaysRun = true)
	protected void beforeMethod() throws Exception {
//		getClient().authenticate();
		getClient().delete("");
	}

	@AfterMethod(alwaysRun = true)
	protected void afterMethod() throws Exception {
		getClient().delete("");
		getClient().disconnect();
	}

	@Override
	protected RemoteDataClient getClient() {
		RemoteDataClient client;
		try {
			client = system.getRemoteDataClient();
			client.authenticate();
			threadClient.set(client);
		} catch (RemoteDataException | RemoteCredentialException | IOException e) {
			Assert.fail("Failed to get client", e);
		}

		return threadClient.get();
	}

	@BeforeClass(alwaysRun=true)
	protected void beforeSubclass() {}


	@AfterClass
	protected void afterClass() throws Exception {
		try {
			getClient().delete("");
			getClient().disconnect();
		} catch (Exception e) {}
	}

	@Override
	@BeforeClass
	public void beforeClass() throws Exception {
		super.beforeClass();

		JSONObject json = getSystemJson();
		json.remove("id");
		json.put("id", this.getClass().getSimpleName());
		system = StorageSystem.fromJSON(json);
		system.setOwner(SYSTEM_USER);
		system.getStorageConfig().setHomeDir("/");
		containerName = system.getStorageConfig().getContainerName();
	}

	@Override
	protected JSONObject getSystemJson() throws JSONException, IOException {
		return jtd.getTestDataObject(STORAGE_SYSTEM_TEMPLATE_DIR + "/" + "s3.example.com.json");
	}

	@Override
    protected void _isPermissionMirroringRequired()
	{
		Assert.assertFalse(getClient().isPermissionMirroringRequired(),
				"S3 permission mirroring should not be enabled.");

	}

	@Override
	protected  void _checksum() {

	}


	@Override
	protected void _isThirdPartyTransferSupported()
	{
		Assert.assertFalse(getClient().isThirdPartyTransferSupported());
	}

	@Override
	protected String getForbiddenDirectoryPath(boolean shouldExist) throws RemoteDataException {
		if (shouldExist) {
			throw new RemoteDataException("Bypassing test for s3 forbidden file/folder");
		} else {
			throw new RemoteDataException("Bypassing test for S3 missing file/folder");
		}
	}

	@Override
	protected void _copyThrowsRemoteDataExceptionToRestrictedSource()
    {
        try
        {
            getClient().copy(MISSING_DIRECTORY, "foo");

            Assert.fail("copy a remote source path that does not exist should throw FileNotFoundException.");
        }
        catch (FileNotFoundException e) {
            Assert.assertTrue(true);
        }
        catch (Exception e) {
            Assert.fail("copy a remote source path that does not exist should throw FileNotFoundException.", e);
        }
    }

    @Override
	protected void _copyThrowsFileNotFoundExceptionOnMissingDestPath() {}

    @Override
	protected void _putFileOutsideHome(String remoteFilename, String expectedRemoteFilename, boolean shouldThrowException, String message) {}

	@Override
	protected void _doRenameThrowsFileNotFoundExceptionOnMissingDestPath() {}
}
