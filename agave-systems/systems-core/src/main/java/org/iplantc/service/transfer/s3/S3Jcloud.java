package org.iplantc.service.transfer.s3;

import static org.jclouds.Constants.PROPERTY_RELAX_HOSTNAME;
import static org.jclouds.Constants.PROPERTY_TRUST_ALL_CERTS;
import static org.jclouds.blobstore.options.PutOptions.Builder.multipart;
import static org.jclouds.s3.reference.S3Constants.PROPERTY_S3_SERVICE_PATH;
import static org.jclouds.s3.reference.S3Constants.PROPERTY_S3_VIRTUAL_HOST_BUCKETS;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.MimetypesFileTypeMap;

import com.sun.org.apache.xpath.internal.operations.Bool;
import javassist.tools.rmi.RemoteException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.iplantc.service.transfer.RemoteDataClient;
import org.iplantc.service.transfer.RemoteFileInfo;
import org.iplantc.service.transfer.RemoteInputStream;
import org.iplantc.service.transfer.RemoteOutputStream;
import org.iplantc.service.transfer.RemoteTransferListener;
import org.iplantc.service.transfer.exceptions.RemoteDataException;
import org.iplantc.service.transfer.model.RemoteFilePermission;
import org.iplantc.service.transfer.model.enumerations.PermissionType;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.ContainerNotFoundException;
import org.jclouds.blobstore.domain.*;
import org.jclouds.blobstore.options.CopyOptions;
import org.jclouds.blobstore.options.ListContainerOptions;
import org.jclouds.http.HttpException;
import org.jclouds.logging.slf4j.config.SLF4JLoggingModule;
import org.jclouds.blobstore.BlobStore;

import com.google.common.collect.ImmutableSet;
import com.google.common.hash.HashCode;
import com.google.common.io.ByteSource;
import com.google.common.io.Files;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.google.inject.Module;

public class S3Jcloud implements RemoteDataClient 
{
	public static final Logger log = Logger.getLogger(S3Jcloud.class);


	protected String cloudProvider;
	protected String rootDir;
	protected String homeDir;
	protected String containerName;
	protected BlobStoreContext context;
	protected BlobStore blobStore;
	
	private String accountKey;
	private String accountSecret;
	private String host;
	private int port = 443;
	private int callCount = 0;

    protected static final int MAX_BUFFER_SIZE = 1*1024*1024;
    
    public S3Jcloud(String accountKey, String accountSecret, String rootDir, String homeDir, String containerName, String host, int port) 
	{
		this.accountSecret = accountSecret;
		this.accountKey = accountKey;
		this.cloudProvider = "s3";
		this.containerName = containerName;
		
		updateEndpoint(host,port);
		
		updateSystemRoots(rootDir, homeDir);


	}

	/* (non-Javadoc)
	 * @see org.iplantc.service.transfer.RemoteDataClient#getHomeDir()
	 */
	@Override
	public String getHomeDir() {
		return this.homeDir;
	}

	/* (non-Javadoc)
	 * @see org.iplantc.service.transfer.RemoteDataClient#getRootDir()
	 */
	@Override
	public String getRootDir() {
		return this.rootDir;
	}	
	
	/* (non-Javadoc)
	 * @see org.iplantc.service.transfer.RemoteDataClient#updateSystemRoots(java.lang.String, java.lang.String)
	 */
	@Override
	public void updateSystemRoots(String rootDir, String homeDir)
	{
		rootDir = FilenameUtils.normalize(rootDir);
        if (!StringUtils.isEmpty(rootDir)) {
			this.rootDir = rootDir;
			if (!this.rootDir.endsWith("/")) {
				this.rootDir += "/";
			}
		} else {
			this.rootDir = "/";
		}

        homeDir = FilenameUtils.normalize(homeDir);
        if (!StringUtils.isEmpty(homeDir)) {
            this.homeDir = this.rootDir +  homeDir;
            if (!this.homeDir.endsWith("/")) {
                this.homeDir += "/";
            }
        } else {
            this.homeDir = this.rootDir;
        }

        this.homeDir = this.homeDir.replaceAll("/+", "/");
        this.rootDir = this.rootDir.replaceAll("/+", "/");
	}
	
	private void updateEndpoint(String host, int port)
	{
	    // TODO: the host should never be null?????
		if (StringUtils.isNotEmpty(host))
		{
		    URL endpoint = null;
			
		    try 
		    {
				endpoint = new URL(host);
				if (port > 0) {
					this.port = port;
				} else if (endpoint.getPort() > 0) {
					this.port = endpoint.getPort();
				} else { 
					if (endpoint.getDefaultPort() > 0) {
						this.port = endpoint.getDefaultPort();					
					} else {
						this.port = 80;
					}
					this.host = host;
					return;
				}
				
				this.host = String.format("%s://%s:%d%s", 
						endpoint.getProtocol(),
						endpoint.getHost(),
						this.port,
						endpoint.getPath());
			}
			catch (Exception e) {}
		} else {
			this.host = null;
			this.port = 443;
		}
	}

	@Override
	public void authenticate() throws IOException, RemoteDataException
	{
		try
		{
			URL endpoint = new URL(host);
			context = ContextBuilder.newBuilder(this.cloudProvider)
					.endpoint(endpoint.toString())
					.credentials(accountKey, accountSecret)
					.buildView(BlobStoreContext.class);
			blobStore = context.getBlobStore();

		} catch (Exception e) {
			throw new RemoteDataException("Failed to parse service endpoint provided in the system.storage.host field.", e);
		}

		// Got rid of creating a bucket automatically here, GET requests should not have side effects like that
		if (!blobStore.containerExists(containerName)) {
			throw new RemoteDataException("Bucket " + containerName + " was not present and could not be created. "
						+ "No data operations can be performed until the bucket is created.");
		}
	}
	

	@Override
	public boolean mkdir(String remotepath)
	throws IOException, RemoteDataException
	{
	    String resolvedPath = _doResolvePath(remotepath);
	    if (resolvedPath == null || resolvedPath.isEmpty()) {
	        return false;
        }
		return _mkdir(remotepath, true);
    }

    public boolean _mkdir(String remotepath, boolean ensureParent) throws RemoteDataException {
	    try {
            String resolvedPath = _doResolvePath(remotepath);
            String parentPath = getParentPath(resolvedPath);

            resolvedPath = StringUtils.removeEnd(resolvedPath, "/");
            if (resolvedPath == null || resolvedPath.isEmpty()) {
                return true;
            }

            // Same behavior as *Nix file systems?
            if (ensureParent && !parentPath.isEmpty()) {
                // Make sure that the parent folder exists

                ListContainerOptions options = new ListContainerOptions();
                options.prefix(parentPath);
                PageSet<? extends StorageMetadata> pageset = blobStore.list(containerName, options);
                if (pageset.size() == 0) {
                    throw new RemoteDataException("Could not create directory, parent folder does not exist");
                }
            }
            blobStore.createDirectory(containerName, resolvedPath);
            return true;
        } catch (Exception e) {
            throw new RemoteDataException("Could not create directory");
        }

    }
	
	@Override
    public boolean mkdirs(String remotepath) 
    throws IOException, RemoteDataException 
    {
	    return _mkdir(remotepath, false);
    }

	@Override
	public boolean mkdirs(String remotepath, String authorizedUsername) 
	throws IOException, RemoteDataException
    {
        return _mkdir(remotepath, false);
    }

	@Override
	public int getMaxBufferSize() {
		return MAX_BUFFER_SIZE;
	}

	@Override
	public RemoteInputStream<?> getInputStream(String remotePath, boolean passive)
	throws IOException, RemoteDataException 
	{
		if (remotePath.isEmpty()) {
			throw new RemoteDataException("Path must be given");
		}
		try 
		{
			String resolvedPath = _doResolvePath(remotePath);
			if (isFile(remotePath)) {
				Blob blob = blobStore.getBlob(containerName, resolvedPath);
				RemoteInputStream istream = new S3InputStream(blob);
				return istream;
			} else {
				throw new RemoteDataException("Cannot open input stream for directory " + remotePath);
			}
		} 
		catch (RemoteDataException e) {
			throw e;
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		catch (Exception e) {
			throw new RemoteDataException("Failed to open input stream to " + remotePath, e);
		}	
	}

	@Override
	public RemoteOutputStream<?> getOutputStream(String remotePath, boolean passive, boolean append)
	throws IOException, RemoteDataException
	{
		// TODO: These should be streams already????
		try 
		{
			String resolvedSrcPath = _doResolvePath(remotePath);
			if (doesExist(resolvedSrcPath))
			{
				if (isDirectory(resolvedSrcPath))
				{
					throw new RemoteDataException("Cannot open output stream to directory " + resolvedSrcPath);
				}
				else
				{
					Blob currentBlob = blobStore.getBlob(containerName, resolvedSrcPath);
					if (currentBlob != null) {
						return new S3OutputStream(this, currentBlob);
					} else {
						throw new RemoteDataException("Failed to open input stream to " + resolvedSrcPath);
					}
				}
			}
			else if (doesExist(getParentPath(resolvedSrcPath)))
			{
				return new S3OutputStream(this, _doResolvePath(resolvedSrcPath));
			}
			else 
			{
				throw new FileNotFoundException("No such file or directory");
			}
		} 
		catch (RemoteDataException e) {
			throw e;
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		catch (Exception e) {
			throw new RemoteDataException("Failed to open input stream to " + remotePath, e);
		}	
	}
	
	public RemoteOutputStream<?> getOutputStream(String remotePath, InputStream in)
	throws IOException, RemoteDataException
	{
		try 
		{
			if (doesExist(remotePath)) 
			{
				if (isDirectory(remotePath))
				{
					throw new RemoteDataException("Cannot open output stream to directory " + remotePath);
				}
				else
				{
					Blob currentBlob = blobStore.getBlob(containerName, _doResolvePath(remotePath));
					if (currentBlob != null) {
						return new S3OutputStream(this, currentBlob);
					} else {
						throw new RemoteDataException("Failed to open output stream to " + remotePath);
					}
				}
			}
			else if (doesExist(getParentPath(remotePath)))
			{
				return new S3OutputStream(this, _doResolvePath(remotePath));
			}
			else 
			{
				throw new FileNotFoundException("No such file or directory");
			}
		} 
		catch (RemoteDataException e) {
			throw e;
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		catch (Exception e) {
			throw new RemoteDataException("Failed to open input stream to " + remotePath, e);
		}	
	}

	@Override
	public List<RemoteFileInfo> ls(String remotepath)
	throws IOException, RemoteDataException
	{
		log.debug("ls");
		try
		{
			List<RemoteFileInfo> listing = new ArrayList<>();
			
			if (isFile(remotepath))
			{
				RemoteFileInfo fileInfo = getFileInfo(remotepath);
				listing.add(fileInfo);
			}
			else
			{
				PageSet<? extends StorageMetadata> pageSet = null;
                // TODO: This should be paginated properly
				// TODO: This could also be added as a utility function, same code in copy()
                do
				{
					log.debug("???");
					String resolvedPath = _doResolvePath(remotepath);
					if (!StringUtils.isEmpty(resolvedPath) && !StringUtils.endsWith(resolvedPath, "/")) {
						resolvedPath += "/";
					}
					
					ListContainerOptions listContainerOptions = new ListContainerOptions();
					if (StringUtils.isNotEmpty(resolvedPath)) {
						listContainerOptions.prefix(resolvedPath);
					}

					if (pageSet != null && pageSet.getNextMarker() != null) {
						listContainerOptions.afterMarker(pageSet.getNextMarker());
					}

					pageSet = blobStore.list(containerName, listContainerOptions);

					for (StorageMetadata storageMetadata: pageSet) {
						if (storageMetadata == null) {
                            continue;
						} else {
						    log.debug(storageMetadata.getName());
						    listing.add(new RemoteFileInfo(storageMetadata));
						}
					}
				} while (pageSet.getNextMarker() != null && pageSet.size() >= 10000);
			}
			
			return listing;	
		} 
		catch (FileNotFoundException e) {
			throw e;
		}
		catch (ContainerNotFoundException e) {
			throw new FileNotFoundException("No such file or directory");
		} 
		catch (RemoteDataException e) {
			throw new RemoteDataException("Failed to list contents of " + remotepath, e);
		}

	}

	@Override
	public void get(String remotedir, String localdir)
	throws IOException, RemoteDataException 
	{
		get(remotedir, localdir, null);
	}

	@Override
	public void get(String remotepath, String localpath, RemoteTransferListener listener) 
	throws IOException, RemoteDataException 
	{
	    // TODO: re-write this
		RemoteFileInfo remoteFileInfo;
    	InputStream in = null;
		try
		{
			remoteFileInfo = getFileInfo(remotepath);
			
			File localDir = new File(localpath);
			
			if (remoteFileInfo.isDirectory())
			{
				if (!localDir.exists()) 
				{
					// if parent is not there, throw exception
					if (!localDir.getParentFile().exists()) {
						throw new FileNotFoundException("No such file or directory");
					}
				} 
				// can't download folder to an existing file
				else if (localDir.isFile()) 
				{
					throw new RemoteDataException("Cannot download file to " + localpath + ". Local path is a file.");
				}
				else
				{
					localDir = new File(localDir, remoteFileInfo.getName());
					
					// create the target directory 
					if (!localDir.mkdir()) {
						throw new IOException("Failed to create local download directory");
					}
				}
				
				if (listener != null) {
					listener.started(0, remotepath);
				}
				
				// recursively copy files into the local folder since irods won't let you specify 
				// the target folder name 
				for (RemoteFileInfo fileInfo : ls(remotepath))
				{
					String remoteChild = remotepath + "/" + fileInfo.getName();
					String localChild = localDir.getAbsolutePath() + "/" + fileInfo.getName();
				
					if (fileInfo.isFile()) 
					{	
						
						Blob blob = blobStore.getBlob(containerName, _doResolvePath(remoteChild));
						if (blob == null) {
							throw new RemoteDataException("Failed to retrieve remote file " + remoteChild );
						} 
						else 
						{
							if (listener != null) {
								listener.started(blob.getMetadata().getContentMetadata().getContentLength(), remoteChild);
							}
							
							File localFile = new File(localChild);
							in = blob.getPayload().openStream();
							FileUtils.copyInputStreamToFile(in, localFile);
							
							if (listener != null) {
								listener.progressed(localFile.length());
							}
							
						}
					}
					else
					{
						get(remoteChild, localChild, listener); 
					}
				}
				
				if (listener != null) {
					listener.completed();
				}
			}
			else 
			{
				if (!localDir.exists()) 
				{
					if(!localDir.getParentFile().exists()) {
						throw new FileNotFoundException("No such file or directory");
					}
				}
				else if (!localDir.isDirectory()) {
					// nothing to do here. handling links
				} else {
					localDir = new File(localDir.getAbsolutePath(), remoteFileInfo.getName());
				}

				Blob blob = blobStore.getBlob(containerName, _doResolvePath(remotepath));
				if (blob == null) {
					throw new RemoteDataException("Failed to get file from " + remotepath );
				} 
				else 
				{
					if (listener != null) {
						listener.started(blob.getMetadata().getContentMetadata().getContentLength(), remotepath);
					}
					
					in = blob.getPayload().openStream();
					FileUtils.copyInputStreamToFile(in, localDir);
					
					if (listener != null) {
						listener.progressed(localDir.length());
						listener.completed();
					}
					
				}
			}
		} 
		catch (FileNotFoundException e) {
			if (listener != null) {
				listener.failed();
			}
			throw new FileNotFoundException("No such file or directory");
		}
		catch (ContainerNotFoundException e) {
			if (listener != null) {
				listener.failed();
			}
			throw new FileNotFoundException("No such file or directory");
		} 
		catch (IOException e) {
			if (listener != null) {
				listener.failed();
			}
			throw e;
		}
		catch (RemoteDataException e) {
			if (listener != null) {
				listener.failed();
			}
			throw e;
		}
		catch (Exception e) {
			if (listener != null) {
				listener.failed();
			}
			throw new RemoteDataException("Failed to copy file from S3.", e);
		}
		finally {
			try { in.close(); } catch (Exception e) {}
		}
	}
	
	/* (non-Javadoc)
     * @see org.iplantc.service.transfer.RemoteDataClient#append(java.lang.String, java.lang.String)
     */
    @Override
    public void append(String localpath, String remotepath) throws IOException,
    RemoteDataException
    {
        append(localpath, remotepath, null);
    }
    
    /* (non-Javadoc)
     * @see org.iplantc.service.transfer.RemoteDataClient#append(java.lang.String, java.lang.String)
     */
    @Override
    public void append(String localpath, String remotepath, RemoteTransferListener listener)  
    throws IOException, RemoteDataException
    {
        File localFile = new File(localpath);
        
        try 
        {
            if (!doesExist(remotepath)) 
            {
                put(localpath, remotepath, listener);
            }
            else if (localFile.isDirectory()) {
                throw new RemoteDataException("cannot append directory");
            }
            else {
                // TODO: implement file appends functionality
                throw new NotImplementedException();
            }
        } 
        catch (IOException e) {
            throw e;
        }
        catch (RemoteDataException e) {
            throw e;
        }
        catch (Exception e) {
            throw new RemoteDataException("Failed to append data to " + remotepath, e);
        }
    }

	@Override
	public void put(String localdir, String remotedir) 
	throws IOException, RemoteDataException 
	{
		put(localdir, remotedir, null);
	}

	@Override
	public void put(String localdir, String remotedir, RemoteTransferListener listener)
 	throws IOException, RemoteDataException 
	{
        // TODO: no need to mkdir, s3 will do that automatically
		try
		{
			File localFile = new File(localdir);
			if (!localFile.exists()) {
				throw new FileNotFoundException("No such file or directory");
			}
			else if (doesExist(remotedir)) 
			{
				// can't put dir to file
				if (localFile.isDirectory() && !isDirectory(remotedir)) {
					throw new RemoteDataException("cannot overwrite non-directory: " + remotedir + " with directory " + localFile.getName());
				} 
				else 
				{
					// TODO: WTF is going on here
					if (remotedir.isEmpty()) {
						remotedir = localFile.getName();
					}
					// remotedir += (StringUtils.isEmpty(remotedir) ? "" : "/") + localFile.getName();

					if (localFile.isDirectory()) {
						mkdir(remotedir);
					}
				}
			}
			else if (doesExist(getParentPath(remotedir)))
			{
				if (localFile.isDirectory())
					mkdir(remotedir);
			}
			else
			{
				// upload and keep name.
				throw new FileNotFoundException("No such file or directory");
			}

			if (localFile.isDirectory()) {
				for (File child: localFile.listFiles()) {

					String childPath = remotedir + "/" + child.getName();
					put(child.getAbsolutePath(), childPath, listener);
				}
			} else {
				String resolvedPath = _doResolvePath(remotedir);
				ByteSource payload = Files.asByteSource(localFile);
				Blob blob = blobStore.blobBuilder(resolvedPath)
						  .payload(payload)
						  .contentLength(localFile.length())
						  .contentType("application/octet-stream")
						  .build();
				
				if (listener != null) {
					listener.started(localFile.length(), remotedir);
				}
				
				blobStore.putBlob(containerName, blob, multipart());
				
				if (listener != null) {
					listener.progressed(localFile.length());
				}
			}
			
			if (listener != null) {
				listener.completed();
			}
		} 
		catch (FileNotFoundException e) {
			if (listener != null) {
				listener.failed();
			}
			throw e;
		} 
		catch (IllegalArgumentException e) {
			if (listener != null) {
				listener.failed();
			}
			throw new RemoteDataException("cannot overwrite non-directory: " + remotedir + " with directory " + localdir);
		} 
		catch (RemoteDataException e) {
			if (listener != null) {
				listener.failed();
			}
			throw e;
		}
		catch (Exception e) {
			if (listener != null) {
				listener.failed();
			}
			throw new RemoteDataException("Remote put failed.", e);
		}

	}
	
	@Override
	public void syncToRemote(String localdir, String remotedir, RemoteTransferListener listener) 
	throws IOException, RemoteDataException
	{
	    // TODO: Rewrite this also
		File sourceFile = new File(localdir);
		if (!sourceFile.exists()) {
			throw new FileNotFoundException("No such file or directory");
		}
		
		try
		{
            if (!doesExist(remotedir)) 
			{
            	put(localdir, remotedir, listener);
				return;
			}
            else if (sourceFile.isDirectory()) 
			{	
				String adjustedRemoteDir = remotedir;
				
				// can't put dir to file
				if (!isDirectory(adjustedRemoteDir)) {
					delete(adjustedRemoteDir);
					put(localdir, adjustedRemoteDir, listener);
					return;
				} 
				else 
				{
					adjustedRemoteDir += (StringUtils.isEmpty(remotedir) ? "" : "/") + sourceFile.getName();
				}
				
				for (File child: sourceFile.listFiles())
				{
					String childRemotePath = adjustedRemoteDir + "/" + child.getName();
					if (child.isDirectory()) 
					{
						// local is a directory, remote is a file. delete remote file. we will replace with local directory
						try {
							if (isFile(childRemotePath)) {
								delete(childRemotePath);
							}
						} catch (FileNotFoundException e) {}
						
						// now create the remote directory
						mkdir(childRemotePath);
						
						// sync the folder now that we've cleaned up
						syncToRemote(child.getAbsolutePath(), adjustedRemoteDir, listener);
					} 
					else
					{
						syncToRemote(child.getAbsolutePath(), childRemotePath, listener);
					}
				}
			} 
			else 
			{
				// sync if file is not there
				if (!doesExist(remotedir))  
				{	
					ByteSource payload = Files.asByteSource(sourceFile);
					String resolvedPath = _doResolvePath(remotedir);
					Blob blob = blobStore.blobBuilder(resolvedPath)
							  .payload(payload)
							  .contentLength(sourceFile.length())
							  .contentType(new MimetypesFileTypeMap().getContentType(sourceFile))
							  .contentMD5((HashCode)null)
							  .build();
					
					if (listener != null) {
						listener.started(sourceFile.length(), remotedir);
					}
					
	                blobStore.putBlob(containerName, blob, multipart());
					
					if (listener != null) {
						listener.progressed(sourceFile.length());
					}
				}
				else 
				{	
				    String resolvedPath = _doResolvePath(remotedir);
					RemoteFileInfo blobMeta = getFileInfoHelper(resolvedPath);
					if (blobMeta == null)
					{
						ByteSource payload = Files.asByteSource(sourceFile);
						Blob blob = blobStore.blobBuilder(resolvedPath)
								  .payload(payload)
								  .contentLength(sourceFile.length())
								  .contentType(new MimetypesFileTypeMap().getContentType(sourceFile))
								  .contentMD5((HashCode)null)
								  .build();
						
						if (listener != null) {
							listener.started(sourceFile.length(), remotedir);
						}
						
	                    blobStore.putBlob(containerName, blob, multipart());
						
						if (listener != null) {
							listener.progressed(sourceFile.length());
						}
					}
					// if the types mismatch, delete remote, use current
					else if (sourceFile.isDirectory() && !blobMeta.isDirectory() ||
							sourceFile.isFile() && !blobMeta.isFile())
					{
						delete(resolvedPath);
						ByteSource payload = Files.asByteSource(sourceFile);
						Blob blob = blobStore.blobBuilder(resolvedPath)
								  .payload(payload)
								  .contentLength(sourceFile.length())
								  .contentType(new MimetypesFileTypeMap().getContentType(sourceFile))
								  .contentMD5((HashCode)null)
								  .build();
						
						if (listener != null) {
							listener.started(sourceFile.length(), remotedir);
						}
						
	                    blobStore.putBlob(containerName, blob, multipart());
						
						if (listener != null) {
							listener.progressed(sourceFile.length());
						}
					}
					// or if the hashes or file sizes are different,  use current
					else if (sourceFile.length() != blobMeta.getSize())
					{
						ByteSource payload = Files.asByteSource(sourceFile);
						Blob blob = blobStore.blobBuilder(resolvedPath)
								  .payload(payload)
								  .contentLength(sourceFile.length())
								  .contentType(new MimetypesFileTypeMap().getContentType(sourceFile))
								  .contentMD5((HashCode)null)
								  .build();
						
						if (listener != null) {
							listener.started(sourceFile.length(), remotedir);
						}
						
	                    blobStore.putBlob(containerName, blob, multipart());
						
						if (listener != null) {
							listener.progressed(sourceFile.length());
						}
					} 
					else 
					{
						log.debug("Skipping transfer of " + sourceFile.getPath() + " to " + 
								remotedir + " because file is present and of equal size.");
					}
				}
				
				if (listener != null) {
					listener.completed();
				}
			}
		} 
		catch (FileNotFoundException e) {
			if (listener != null) {
				listener.failed();
			}
			throw e;
		} 
		catch (IllegalArgumentException e) {
			if (listener != null) {
				listener.failed();
			}
			throw new RemoteDataException("cannot overwrite non-directory: " + remotedir + " with directory " + localdir);
		} 
		catch (RemoteDataException e) {
			if (listener != null) {
				listener.failed();
			}
			throw e;
		}
		catch (Exception e) {
			if (listener != null) {
				listener.failed();
			}
			throw new RemoteDataException("Remote put failed.", e);
		}
	}

	@Override
	public boolean isDirectory(String path)
 	throws IOException, RemoteDataException 
	{
		return getFileInfo(path).isDirectory();
	}

	@Override
	public boolean isFile(String path)
	throws IOException, RemoteDataException
	{
		return getFileInfo(path).isFile();
	}

	@Override
	public long length(String path)
 	throws IOException, RemoteDataException 
	{
		return getFileInfo(path).getSize();
	}

	@Override
	public String checksum(String remotepath)
 	throws IOException, RemoteDataException, NotImplementedException
	{
		try
		{
			if (isDirectory(remotepath)) {
				throw new RemoteDataException("Directory cannot be checksummed.");
			} else {
				throw new NotImplementedException();
			}
		}
		catch (IOException e) {
			throw e;
		}
		catch (RemoteDataException e) {
			throw e;
		}
		catch (NotImplementedException e) {
			throw e;
		}
	}

	@Override
	public void doRename(String srcPath, String destPath) 
	throws IOException, RemoteDataException 
	{
		doCopy(srcPath, destPath, null, true);
	}

	@Override
	public void copy(String remotedir, String localdir)  
	throws IOException, RemoteDataException 
	{
        doCopy(remotedir, localdir, null, false);
	}

	@Override
	public void copy(String srcPath, String destPath, RemoteTransferListener listener) 
	throws IOException, RemoteDataException 
	{
		doCopy(srcPath, destPath, listener, false);
	}
	
	@SuppressWarnings({ "unused", "deprecation" })
	private void doCopy(String srcPath, String destPath, RemoteTransferListener listener, boolean deleteSource)
	throws IOException, RemoteDataException 
	{
		//foo => bar = bar/foo when bar exists
		//foo/ => bar = bar when bar exists
		//foo => bar/ = bar/foo when bar exists

		// Can't do anything if the source or dest are null
		if (srcPath ==null || destPath ==null) {
			if (listener != null) {
				listener.failed();
			}
			throw new RemoteDataException("Source or Destination path must not be null");
		}

		// Can't do anything if the source or dest are equal
		if (srcPath == destPath) {
			if (listener != null) {
				listener.failed();
			}
			throw new RemoteDataException("Source or Destination path are same");
		}

		RemoteFileInfo sourceFileInfo;
		try  
		{
			String resolvedSourcePath = _doResolvePath(srcPath); 
			String resolvedDestPath = _doResolvePath(destPath);

			String destParentPath = getParentPath(resolvedDestPath);
			if (!doesExist(destParentPath)) {
				throw new FileNotFoundException("Parent path does not exist");
			}
			sourceFileInfo = getFileInfo(resolvedSourcePath);
			
			if (sourceFileInfo.isFile()) 
			{
				if (doesExist(destPath)) 
				{
				    if (!isFile(destPath)) {
						throw new RemoteDataException("Cannot rename a file to an existing directory path.");
					}
				}
			} 
			else if (doesExist(resolvedDestPath))
			{
				if (isFile(resolvedDestPath)) {
					throw new RemoteDataException("Cannot rename a directory to an existing file path.");
				}
				else {
					if (StringUtils.isEmpty(destPath)) {
						resolvedDestPath = _doResolvePath(sourceFileInfo.getName());
					}
				}
			}

        	if (sourceFileInfo.isDirectory())
        	{
        		// TODO: This is basically untested
        		mkdir(resolvedDestPath);
                PageSet<? extends StorageMetadata> pageset = null;
                do 
        		{
        			ListContainerOptions options = new ListContainerOptions();
        			options.prefix(resolvedSourcePath);
        			options.withDetails();
        			options.recursive();
        			if (pageset != null && StringUtils.isEmpty(pageset.getNextMarker())) {
        				options.afterMarker(pageset.getNextMarker());
        			}
        			
        			pageset = blobStore.list(containerName, options);

        			// Keep track of the blob names, can delete them all in one batch if needed
        			ArrayList<String> blobNames = new ArrayList<>();

        			// if we have /a/b/c/d/e/
					// and copy /a/b/c to /cat/dog/
					// we want /cat/dog/c/d/e/

        			for (StorageMetadata meta: pageset) {
        				if (meta == null) continue;

        				// just need the innermost folders name, /a/b/c/d => d
						String targetFolder = FilenameUtils.getBaseName(resolvedSourcePath);
        				String destSubPath = StringUtils.removeStart(meta.getName(), resolvedSourcePath);
						String destTargetPath = resolvedDestPath + "/" + targetFolder + "/" + destSubPath;
						destTargetPath = FilenameUtils.normalize(destTargetPath);
                        if (listener != null) {
                            listener.started(((BlobMetadata)meta).getContentMetadata().getContentLength(), destTargetPath + "/" + meta.getName());
                        }
                        blobStore.copyBlob(containerName, meta.getName(), containerName, destTargetPath, CopyOptions.NONE);
						blobNames.add(meta.getName());
                        if (listener != null) {
                            listener.progressed(((BlobMetadata)meta).getContentMetadata().getContentLength());
                        }
        			}

        			if (deleteSource) {
        				blobStore.removeBlobs(containerName, blobNames);
					}
        		} 
        		while (pageset.getNextMarker() != null && pageset.size() >= 10000);
        	} else {
        		// Just a blob, can basically just change prefix
        		blobStore.copyBlob(containerName, resolvedSourcePath, containerName, resolvedDestPath, CopyOptions.NONE);
			}
            


		}
		catch (RemoteDataException | IOException e) {
			if (listener != null) {
				listener.failed();
			}
			throw e;
		}
		catch (Throwable e) {
			if (listener != null) {
				listener.failed();
			}
			throw new RemoteDataException(String.format("Internal error when attempting to copy %s to %s on the remote server.", srcPath, destPath), e);
		}


	}

	@Override
	public URI getUriForPath(String path)
	throws IOException, RemoteDataException 
	{
	    //TODO: This should not only point to s3://
		try
		{
			return new URI("s3://" + host + (port == 80 || port == 443 ? "" : ":" + port) + "/" + path);
		}
		catch (URISyntaxException e)
		{
			throw new IOException(e);
		}
	}

	@Override
	public void delete(String path)
    throws IOException, RemoteDataException
	{
        try {
			String resolvedPath = _doResolvePath(path);
			String resolvedParentPath = getParentPath(resolvedPath);
			ListContainerOptions opts = new ListContainerOptions();
			if (!doesExist(resolvedPath)) {
				throw new FileNotFoundException("File/Folder not found");
			}
			//TODO: use prefix after jClouds 2.2, this is hacky and gross
			if (isDirectory(resolvedPath)) {
				opts.recursive();
				opts.inDirectory(resolvedPath);
				blobStore.clearContainer(containerName, opts);
				if (!resolvedPath.isEmpty()){
					try {
						blobStore.deleteDirectory(containerName, resolvedPath);
					} catch (Exception e) {
						log.debug(e.getStackTrace());
					}
//                    blobStore.deleteDirectory(containerName, resolvedPath);
					if (!resolvedParentPath.isEmpty()) {
                        blobStore.createDirectory(containerName, resolvedParentPath);
                    }
				}

			} else {
				blobStore.removeBlob(containerName, resolvedPath);
			}


        }
        catch (ContainerNotFoundException e) {
			throw new FileNotFoundException("No such file or directory");
		}
		catch (FileNotFoundException e) {
        	throw e;
		}
        catch (Exception e)
		{
			throw new RemoteDataException("Failed to delete " + path, e);
		}
	}

	@Override
	public boolean isThirdPartyTransferSupported() 
	{
		// TODO: support server side copies
		return false;
	}

	@Override
	public void disconnect() {
		if (context != null) {
			context.close();
		}
		blobStore = null;
		context = null;
	}

	@Override
	public boolean doesExist(String remotePath) 
	throws IOException, RemoteDataException 
	{
		String resolvedPath;
		
		try 
		{
			resolvedPath = _doResolvePath(remotePath);
			if (StringUtils.isEmpty(resolvedPath)) {
				return blobStore.containerExists(containerName);
			} 
			else
			{
//				Boolean dirExists = blobStore.directoryExists(containerName, resolvedPath);
				Boolean blobExists = blobStore.blobExists(containerName, resolvedPath);
				if (blobExists) {
					return true;
				} else {
					// Could also be an empty folder, which does not come back with metadata in HEAD request
					ListContainerOptions opts = new ListContainerOptions();
					opts.prefix(resolvedPath);
					PageSet<? extends StorageMetadata> pageset;
					pageset = blobStore.list(containerName, opts);
					log.debug(pageset.size() > 0);
					return pageset.size() > 0;
				}
			}
		}
		catch (HttpException e) {
			return false;
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		catch (ContainerNotFoundException e) {
			throw new FileNotFoundException("No such file or directory");
		} 
		catch (UncheckedExecutionException e) {
		    if (e.getCause() instanceof org.jclouds.rest.AuthorizationException) {
		        return true;
		    } else {
		        throw new RemoteDataException("Failed to retrieve information for " + remotePath, e);
		    }
		}
		catch (Exception e)
		{
			throw new RemoteDataException("Failed to retrieve information for " + remotePath, e);
		}

	}

	protected String _doResolvePath(String path) throws FileNotFoundException {
	    return StringUtils.stripEnd(resolvePath(path), "/");
	}
	
	@Override
    public String resolvePath(String path) throws FileNotFoundException {
		if (StringUtils.isEmpty(path)) {
		    return homeDir;
//			return StringUtils.removeStart(homeDir, "/");
		}
		else if (path.startsWith("/")) 
		{
			path = rootDir + path.replaceFirst("/", "");
		}
		else
		{
			path = homeDir + path;
		}
		
		String adjustedPath = path;
		if (adjustedPath.endsWith("/..") || adjustedPath.endsWith("/.")) {
			adjustedPath += File.separator;
		}
		
		if (adjustedPath.startsWith("/")) {
		    path = org.codehaus.plexus.util.FileUtils.normalize(adjustedPath);
		} else {
			path = FilenameUtils.normalize(adjustedPath);
		}
		
		if (path == null) {
			throw new FileNotFoundException("The specified path " + path + 
					" does not exist or the user does not have permission to view it.");
		} else if (!path.startsWith(rootDir)) {
			if (!path.equals(StringUtils.removeEnd(rootDir, "/"))) {
				throw new FileNotFoundException("The specified path " + path + 
					" does not exist or the user does not have permission to view it.");
			}
		}
		path = StringUtils.removeStart(path, "/");
		return path;
	}
	
	public String getParentPath(String path) {
        File f = new File(path);
        return f.getParent() == null ? "" : f.getParent();
	}


	/**
     * Convenience method to work around AWS being picky about trailing slashes.
     * @param resolvedPath
     * @return
     * @throws FileNotFoundException
     * @throws RemoteDataException 
     */
    private RemoteFileInfo getFileInfoHelper(String resolvedPath)
	throws FileNotFoundException, RemoteDataException
	{
        StorageMetadata meta = blobStore.blobMetadata(containerName, resolvedPath);
        if (meta != null) {
            return new RemoteFileInfo(meta);
        } else {
            PageSet<? extends StorageMetadata> pageSet;
            ListContainerOptions options = new ListContainerOptions();
            options.prefix(resolvedPath);
            pageSet = blobStore.list(containerName, options);
            if (pageSet.size() > 0) {
            	String fname = pageSet.iterator().next().getName();
                RemoteFileInfo fileInfo;
                fileInfo = new RemoteFileInfo();
                fileInfo.setFileType(RemoteFileInfo.DIRECTORY_TYPE);
                fileInfo.setLastModified(new Date());
                fileInfo.setOwner(RemoteFileInfo.UNKNOWN_STRING);
                fileInfo.setSize(0);
                fileInfo.setName(fname);
                return fileInfo;
            } else {
                return null;
            }
        }
	}


	@Override
	public RemoteFileInfo getFileInfo(String path) 
	throws RemoteDataException, IOException 
	{
		callCount++;
		log.debug("CALL COUNT IS " + callCount);
	    String resolvedPath = _doResolvePath(path);
		try 
		{
			RemoteFileInfo fileInfo;
			if (StringUtils.isEmpty(resolvedPath)) {
				if (blobStore.containerExists(containerName)) {
					fileInfo = new RemoteFileInfo();
					fileInfo.setName("/");
					fileInfo.setFileType(RemoteFileInfo.DIRECTORY_TYPE);
					fileInfo.setLastModified(new Date());
					fileInfo.setOwner(RemoteFileInfo.UNKNOWN_STRING);
					fileInfo.setSize(0);
				} else {
					throw new FileNotFoundException("No such file or directory");
				}
			} 
			else
			{
				fileInfo = getFileInfoHelper(path);
				if (fileInfo == null)
				{
					throw new FileNotFoundException("No such file or directory");
				}
			}
			
			return fileInfo;
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		catch (ContainerNotFoundException e) {
			throw new FileNotFoundException("No such file or directory");
		} 
		catch (Exception e)
		{
			throw new RemoteDataException("Failed to retrieve information for " + path, e);
		}
	}

	@Override
	public String getUsername() {
		return this.accountKey;
	}

	@Override
	public String getHost() {
		return this.host;
	}

	@Override
	public List<RemoteFilePermission> getAllPermissionsWithUserFirst(String path, String username) 
	throws RemoteDataException 
	{
		
		return Arrays.asList(new RemoteFilePermission(username, null, PermissionType.ALL, true));
	}

	@Override
	public List<RemoteFilePermission> getAllPermissions(String path)
	throws RemoteDataException 
	{
		return new ArrayList<RemoteFilePermission>();
	}

	@Override
	public PermissionType getPermissionForUser(String username, String path)
	throws RemoteDataException 
	{	
		return PermissionType.ALL;
	}

	@Override
	public boolean hasReadPermission(String path, String username)
	throws RemoteDataException 
	{
		return true;
	}

	@Override
	public boolean hasWritePermission(String path, String username)
	throws RemoteDataException 
	{
		return true;
	}

	@Override
	public boolean hasExecutePermission(String path, String username)
	throws RemoteDataException 
	{
		return true;
	}

	@Override
	public void setPermissionForUser(String username, String path, PermissionType type, boolean recursive) 
	throws RemoteDataException 
	{
		
	}

	@Override
	public void setOwnerPermission(String username, String path, boolean recursive) 
	throws RemoteDataException 
	{	
	}

	@Override
	public void setReadPermission(String username, String path, boolean recursive)
	throws RemoteDataException 
	{
	}

	@Override
	public void removeReadPermission(String username, String path, boolean recursive)
	throws RemoteDataException 
	{	
	}

	@Override
	public void setWritePermission(String username, String path, boolean recursive)
	throws RemoteDataException 
	{	
	}

	@Override
	public void removeWritePermission(String username, String path, boolean recursive)
	throws RemoteDataException 
	{	
	}

	@Override
	public void setExecutePermission(String username, String path, boolean recursive)
	throws RemoteDataException 
	{
	}

	@Override
	public void removeExecutePermission(String username, String path, boolean recursive) 
	throws RemoteDataException 
	{
	}

	@Override
	public void clearPermissions(String username, String path, boolean recursive)
	throws RemoteDataException 
	{
	}

	@Override
	public String getPermissions(String path) throws RemoteDataException 
	{
		RemoteFileInfo file;
		try 
		{
			file = getFileInfo(path);
			
			if (file == null) {
				throw new RemoteDataException("No file found at " + path);
			} else {
				return file.getPermissionType().name();
			}
		} 
		catch (IOException e) 
		{
			throw new RemoteDataException("No file found at " + path);
		}
	}

	@Override
	public boolean isPermissionMirroringRequired() {
		return false;
	}
}
